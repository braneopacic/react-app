import React, { Component } from 'react';
import './Person.css';
import PropTypes from 'prop-types';


class Person extends Component {

    constructor(props){
        super(props);
        console.log('Person.js inside constructor ', props);
    }

    componentWillMount() {
        console.log('Person.js inside componentWillMount() ');
    }

    componentDidMount() {
        console.log('Person.js inside componentDidMount() ');
        if(this.props.position === 1) {
            this.inputElement.focus();
        }
        
    }



    render() {

        console.log('Person.js inside render() ');

        return(

            <div  className="Person">
                <p onClick={this.props.click} >I'm {this.props.name}, and I'm {this.props.age} years old.</p>
                <p>{this.props.children}</p>
                <input type="text" 
                ref = {(inp) => this.inputElement = inp }
                onChange={this.props.change} 
                value={this.props.name}/>
            </div>

        )
    }
}

Person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    change: PropTypes.func
}


export default Person;




