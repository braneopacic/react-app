import React, { PureComponent } from 'react';
import Person from './Person/Person';


class Persons extends PureComponent {

    constructor(props){
        super(props);
        console.log('Persons.js ----- inside constructor ', props);
    }

    componentWillMount() {
        console.log('Persons.js ----- inside componentWillMount() ');
    }

    componentDidMount() {
        console.log('Persons.js ----- inside componentDidMount() ');
    }

    // Component lifecycle on UPDATE - (triggered by Parent - props change)
    componentWillReceiveProps(nextProps) {
        console.log('[UPDATE] Persons.js // Inside componentWillReciveProps ', nextProps);
    }

    // ommited shouldComponentUpdate bc we imported PureComponent instead of Component
    // PureComponent automatically does this check

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('[UPDATE] Persons.js // Inside shouldComponentUpdate ', nextProps, nextState);
    //     // if something changes, it returns true and the cycle goes on
    //     return nextProps.persons !== this.props.persons ||
    //             nextProps.clicked !== this.props.clicked ||
    //             nextProps.changed !== this.props.changed; 
    //     // return true;
    // }

    componentWillUpdate(nextProps, nextState) {
        console.log('[UPDATE] Persons.js // Inside componentWillUpdate ', nextProps, nextState);
    }

    componentDidUpdate() {
        console.log('[UPDATE] Persons.js // Inside componentDidUpdate ');
    }


    render() {

        console.log('Persons.js ------ inside render() ');

        return (
            this.props.persons.map((person, index) => {
                return (
                  <Person
                  click={this.props.clicked.bind(this, index)} 
                  position = {index}
                  name={person.name}
                  age={person.age}
                  key={person.id}
                  change={(e) => this.props.changed(e, person.id)}
                  />
                  
        )
    }
))

}
}





export default Persons;