import React from 'react';
import Auxx from '../hoc/Auxx';

const cockpit = (props) => {
    return(
        <Auxx>
            <h1>{props.title}</h1>
            <p className={props.classes.join(' ')}>This is actually working!</p>
            <button  
            style = {props.style}
            onClick={props.togglePersonsHandler}>{props.showHidePersons}</button> 
        </Auxx>
    )
}





export default cockpit;