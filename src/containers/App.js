import React, { PureComponent } from 'react';
import './App.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../Cockpit/Cockpit';
import WithClass from '../hoc/WithClass';

class App extends PureComponent {

  constructor(props){
    super(props);
    console.log("App.js inside constructor: ", props);
  }


  componentWillMount() {
    console.log("App.js Inside componentWillMount() ");
  }

  componentDidMount() {
    console.log('App.js Inside componentDidMount() ');
  }

  // UPDATE compontent lifecycle 

  componentWillReceiveProps(nextProps) {
    console.log('[UPDATE App.js] inside componentWillReceiveProps ', nextProps);
  }

  // using PureComponent, shouldComponentUpdate should be omitted then.

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('[UPDATE App.js] inside shouldComponentUpdate ', nextProps, nextState);
  //   return nextState.persons !== this.state.persons ||
  //   nextState.showPersons !== this.state.showPersons;
  // }

  state = {
    persons: [
      { id: 'asd', name: "Max", age: 28 },
      { id: 'adadads', name: "Manu", age: 29 },
      { id: 'ddadas21', name: "Stephanie", age: 26 }
    ],
    // this property of persons (of state) wont get interfered by this.setState() on event-handler method
    otherState: 'some random value',
    showPersons: false,
    counterValue: 0
  }





  changeState = (e, id) => {


    const getTheIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    })


    const getTheCopyOfPerson = {
      ...this.state.persons[getTheIndex]
    };

    getTheCopyOfPerson.name = e.target.value;

    const getTheCopyOfTheArray = [...this.state.persons];


    getTheCopyOfTheArray[getTheIndex] = getTheCopyOfPerson;

    this.setState({
      persons: getTheCopyOfTheArray
    })


  }



  deletePersonHandler = (personIndex) => {
    const personel = [...this.state.persons];
    personel.splice(personIndex, 1);
    this.setState({
      persons: personel
    })
  }


  togglePersonsHandler = () => {

    const opositeShowPersons = this.state.showPersons;


    // mutating state via relying on prev state (bc it works async)
    // is using an arrow function via prevState and with it we update the
    // newer state of property, referencing it with older 
    this.setState((prevState, props) => {
      return {
        showPersons: !opositeShowPersons,
        counterValue: prevState.counterValue + 1
      }
    })
  }

  render() {

    console.log('App.js inside render() ');

    const style = {
      backgroundColor: 'green',
      font: 'inherit',
      border: '2px solid blue',
      padding: '8px',
      cursor: 'pointer',
      transition: 'all 0.6s ease-in',
      
    }

    let showHidePersons = (
      <span>Show persons</span>
    );

    if(this.state.showPersons) {
      showHidePersons = (
        <span>Hide persons</span>
      )
    }
    let persons = null;

    if(this.state.showPersons) {
      persons = (
        <div>
          
          <Persons 
          persons={this.state.persons}
          clicked={this.deletePersonHandler}
          changed={this.changeState} />
        </div>
      )

      style.backgroundColor = 'red';
      
    }

    const classes = [];

    if(this.state.persons.length >= 2){
      classes.push('red');
    }

    if(this.state.persons.length >= 1) {
      classes.push('bold');
    }
    


    return (
      
        <WithClass classes="App">
          <button 
          onClick={() => this.setState({showPersons: true})}
          >Show Persons</button>
          <Cockpit
          title={this.props.titleApp} 
          style={style}
          classes={classes}
          showHidePersons = {showHidePersons}
          togglePersonsHandler = {this.togglePersonsHandler} />    
          {persons}
        </WithClass>
      
    );

    // return React.createElement("div", {className: "App"}, React.createElement("h1", null, "Does this work?"));
  }
}





export default App;
